#include <Arduino.h>
#include <RTClib.h>
#include <SPI.h>
#include <SD.h>
#include <SparkFun_MMA8452Q.h>

RTC_PCF8523 rtc;
const int sdChipSelect = 10;
char filename[20];
MMA8452Q accel;

#define FREQUENCY_MS		500
#define ACCELDATA_LENGTH	8

struct accelData {
	unsigned long ts;
	float cx, cy, cz;
	float dx, dy, dz;
	byte pl;
};

struct accelData accelData[ACCELDATA_LENGTH];
byte accelIndex;

bool init_rtc() {
	Serial.print("Initializing RTC... ");
	if (!rtc.begin()) {
		Serial.println("Couldn't find RTC");
		return false;
	}

	if (! rtc.initialized()) {
		Serial.println("RTC is NOT running!");
		return false;
	}

	Serial.println("RTC initialized.");
	return true;
}

bool init_sd_card() {
	Serial.print("Initializing SD card... ");
	// see if the card is present and can be initialized:
	if (!SD.begin(sdChipSelect)) {
		Serial.println("Card failed, or not present");
		// don't do anything more:
		return false;
	}

	Serial.println("card initialized.");
	return true;
}

bool init_accel() {
	Serial.print("Initializing accelerometer... ");
	accel.init();
	accelData[0] = {0, 0, 0, 0, 0, 0, 0, 0};
	accelIndex = 1;
	Serial.println("accelerometer initialized.");
	return true;
}

void setup() {
	DateTime now;

	while (!Serial)
		delay(1);

	Serial.begin(115200);

	if (!init_rtc())
		while(true);

	if (!init_sd_card())
		while(true);

	if (!init_accel())
		while(true);

	now = rtc.now();
	sprintf(filename, "%02d%02d%02d%02d.log", now.month(), now.day(),
				now.hour(), now.minute());
	Serial.print(millis());
	Serial.print(" ");
	Serial.println(filename);
}

void loop() {
	File dataFile;

	if (accel.available()) {
		byte prevIndex;

		accel.read();

		if (accelIndex == 0)
			prevIndex = ACCELDATA_LENGTH - 1;
		else
			prevIndex = accelIndex - 1;
		accelData[accelIndex] = {millis(), accel.cx, accel.cy, accel.cz,
							accel.cx - accelData[prevIndex].cx,
							accel.cy - accelData[prevIndex].cy,
							accel.cz - accelData[prevIndex].cz,
							accel.readPL()};
		accelIndex++;
	}

	if (accelIndex >= ACCELDATA_LENGTH) {
		char str[64];
		char cxStr[8], cyStr[8], czStr[8];
		char dxStr[8], dyStr[8], dzStr[8];

		dataFile = SD.open(filename, FILE_WRITE);
		if (!dataFile) {
			Serial.print("Unable to open log file ");
			Serial.println(filename);
		} else {
			for (accelIndex = 0; accelIndex < ACCELDATA_LENGTH ; accelIndex++) {
				dtostrf(accelData[accelIndex].cx, 4, 3, cxStr);
				dtostrf(accelData[accelIndex].cy, 4, 3, cyStr);
				dtostrf(accelData[accelIndex].cz, 4, 3, czStr);
				dtostrf(accelData[accelIndex].dx, 4, 3, dxStr);
				dtostrf(accelData[accelIndex].dy, 4, 3, dyStr);
				dtostrf(accelData[accelIndex].dz, 4, 3, dzStr);
				snprintf(str, 64, "%ld - %s %s %s - %s %s %s - %d",
							accelData[accelIndex].ts,
							cxStr, cyStr, czStr, dxStr, dyStr, dzStr,
							accelData[accelIndex].pl);
				Serial.println(str);
				dataFile.println(str);
			}
			dataFile.close();
		}
		accelIndex = 0;
	}

	delay(FREQUENCY_MS - (millis() % FREQUENCY_MS));
}
